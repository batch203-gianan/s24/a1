//console.log(`Hello World`);
const getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);

const address = ["258", "Washington Ave NW", "California", "90011"];

const [houseNumber, avenue, state, zipCode] = address;
console.log(`I live at ${houseNumber} ${avenue} ${state} ${zipCode}`);

const animal = {
	name: "Lolong",
	kind: "salt water crocodile",
	weight: "1075",
	measurement: "20 ft 30 in"
};
const {name, kind, weight, measurement} = animal;
console.log(`${name} was a ${kind}. He weighed at ${weight} kgs with a measurement of ${measurement}.`);

const numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => console.log(`${number}`));
const sum = numbers.reduce((acc, cur) => acc + cur,0);
console.log(sum);

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
};

let myDog = new Dog("Frankie", 25, "Miniature Dachshund");
console.log(myDog);


